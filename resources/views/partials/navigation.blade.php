<nav class="navbar navbar-default navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#menu-nav-collapse">
                <span class="sr-only">Visa navigering</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('') }}">Företag AB</a>
        </div>

        <div class="collapse navbar-collapse" id="menu-nav-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ is_active('/') }}">
                    <a href="{{ url('') }}">
                    @if (Auth::check() && Auth::user()->admin)
                        <i class="fa fa-tachometer"></i> Panel
                    @else
                        <i class="fa fa-home"></i> Hem
                    @endif
                    </a>
                </li>
                <li class="{{ is_active('about') }}">
                    <a href="{{ url('about') }}">Om</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li class="{{ is_active('auth/login') }}">
                        <a href="{{ url('auth/login') }}"><i class="fa fa-sign-in"></i> Logga in</a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            <i class="fa fa-caret-down"></i> {{ Auth::user()->name }}
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Logga ut</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>