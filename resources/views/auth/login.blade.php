@extends('base')

@section('title') Logga in :: @parent @stop

@section("content")
<div class="row">
    <div class="page-header">
        <h2>Logga in</h2>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        
        @include('errors.list')

        <form class="form-horizontal" role="form" method="POST" action="{{ url('auth/login') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">E-post adress</label>

                <div class="col-md-4">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="col-md-4 control-label">Lösenord</label>

                <div class="col-md-4">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Kom ihåg mig
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary" style="margin-right: 15px">
                        Logga in
                    </button>

                    <a href="{{ url('password/email') }}">Glömt lösenord</a>
                </div>
            </div>
        </form>
    </div>
</div>

@stop