<div class="hidden">
    <div id="create-user-container" style="overflow-x: hidden; height: 100%">
        <div class="container-fluid">
            <div class="page-header">
                <h3>Skapa användare</h3>
            </div>
            <div class="row">
                <form id="create-user-form" class="form-horizontal" role="form" method="POST" action="{{ url('admin/users/store') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Namn</label>
    
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label for="email" class="col-md-4 control-label">E-post adress</label>
    
                        <div class="col-md-6">
                            <input type="email" name="email" class="form-control">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Generera lösenord</label>

                        <div class="col-md-6">
                            <input type="text" id="generated-password" class="form-control generate-control">
                            <button type="button" id="generate-password" class="btn btn-default">
                                Generera
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-4 control-label">Lösenord</label>
    
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation" class="col-md-4 control-label">Bekräfta lösenord</label>
    
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <h4>Avdelningar</h4>
                        </div>
                        <div class="col-md-6 col-md-offset-4">
                            <input type="text" id="sections-list" name="sections" style="width: 100%" placeholder="Sök...">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="submit" class="btn btn-primary">Skapa</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>