<div class="hidden">
    <div id="delete-section-container">
        <div class="container-fluid">
            <div class="page-header">
                <h3>Ta bort avdelning</h3>
            </div>
            <input type="hidden" id="delete-token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="section-id">
            <p>Är du säker på att du vill ta bort avdelningen <span class="section-name"></span>?</p>
            <div class="form-group">
                <button class="confirm-delete btn btn-danger">Ta bort</button>
                <button class="cancel-delete btn btn-default">Avbryt</button>
            </div>
        </div>
    </div>
</div>