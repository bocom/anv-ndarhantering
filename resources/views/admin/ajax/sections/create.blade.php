<div class="hidden">
    <div id="create-section-container" style="overflow-x: hidden; height: 100%">
        <div class="container-fluid">
            <div class="page-header">
                <h3>Skapa avdelning</h3>
            </div>
            <div class="row">
                <form id="create-section-form" class="form-horizontal" role="form" method="POST" action="{{ url('admin/sections/store') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Namn</label>
    
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <h4>Användare</h4>
                        </div>
                        <div class="col-md-6 col-md-offset-4">
                            <input type="text" id="users-list" name="users" style="width: 100%" placeholder="Sök...">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="submit" class="btn btn-primary">Skapa</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>