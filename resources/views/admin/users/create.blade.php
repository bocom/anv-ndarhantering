@extends('admin.layouts.base')

@section('styles')
@parent
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div id="create-user-container" style="overflow: hidden">
    <div class="container-fluid">
        <div class="page-header">
            <h3>Skapa användare</h3>
        </div>
        <div class="row">
            @include('errors.list')

            <form id="create-user-form" class="form-horizontal" role="form" method="POST" action="{{ url('admin/users/store') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Namn</label>

                    <div class="col-md-6">
                        <input type="text" name="name" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">E-post adress</label>

                    <div class="col-md-6">
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Generera lösenord</label>

                    <div class="col-md-6">
                        <input type="text" id="generated-password" class="form-control generate-control">
                        <button type="button" id="generate-password" class="btn btn-default">
                            Generera
                        </button>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">Lösenord</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password_confirmation" class="col-md-4 control-label">Bekräfta lösenord</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <h4>Avdelningar</h4>
                    </div>
                    <div class="col-md-6 col-md-offset-4">
                        <input type="text" id="sections-list" name="sections" style="width: 100%" placeholder="Sök...">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-5">
                        <button type="submit" class="btn btn-primary">Skapa</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var sections = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.nonword(d.value)
        },
        queryTokenizer: Bloodhound.tokenizers.nonword,
        identify: function (obj) { return obj.value; },
        remote: {
            url: "{{ url('admin/sections/find/%QUERY') }}",
            wildcard: "%QUERY",
            filter: function (response) {
                var tagged_user = $('#sections-list').tokenfield('getTokens');
                return $.map(response, function (user) {
                    var exists = false;
                    for (i=0; i < tagged_user.length; i++) {
                        if (user.id == tagged_user[i].value) {
                            var exists = true;
                        }
                    }
                    if (!exists) {
                        return {
                            value: user.id,
                            label: user.name
                        };
                    }
                });
            }
        }
    });
    sections.initialize();

    $("#sections-list").tokenfield({
        typeahead: [
            {
               hint: false, 
            }, { name: 'sections', displayKey: 'label', source: sections.ttAdapter() }]
    })
    .on('tokenfield:createtoken', function (e) {
        var existingTokens = $(this).tokenfield('getTokens');
        if (existingTokens.length) {
            $.each(existingTokens, function(index, token) {
                if (token.value === e.attrs.value) {
                    e.preventDefault();
                }
            });
        }
    });
</script>
@stop