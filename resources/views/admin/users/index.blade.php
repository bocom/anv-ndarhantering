@extends('admin.layouts.base')

@section('title') Användare :: @parent @stop

@section('styles')
@parent
<link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" href="//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div class="page-header">
    <h3>
        Användare
        <div class="pull-right">
            <a href="{{ url('admin/users/create') }}" class="btn btn-sm btn-primary create-user" title="Ny användare">
                <i class="fa fa-user-plus"></i>
                <span class="hidden-sm text">Ny användare</span>
            </a>
        </div>
    </h3>
</div>
<table id="table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Namn</th>
            <th>E-post</th>
            <th>Åtgärder</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
@include('admin.ajax.users.create')
@include('admin.ajax.users.delete')
@stop

@section('scripts')
@parent
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var oTable;
    $(document).ready(function () {
        oTable = $("#table").DataTable({
            ajax: "{{ url('admin/users/datatable') }}",
            drawCallback: function () {
                $(".delete-user").colorbox({
                    inline: true,
                    width: 500,
                    height: 275,
                    href: "#delete-user-container",
                    onOpen: function () {
                        var el = $.colorbox.element();
                        var id = el.data('id');
                        var name = el.parent().prev().prev().html();
                        $(".user-name").html(name);
                        $(".user-id").val(id);
                    },
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Swedish.json"
            },
            processing: true,
            responsive: true,
            sDom: "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            serverSide: true,
            columnDefs: [
                { "searchable": false, "targets": 2 }
            ]
        });
        $(".create-user").colorbox({
            inline: true,
            width: "40%",
            height: 620,
            href: "#create-user-container",
            onClosed: function () {
                oTable.ajax.reload();
            }
        });
        
        $("#create-user-form").submit(function (ev) {
            ev.preventDefault();

            var data = $("#create-user-form").serialize();

            $.ajax({
                type: 'POST',
                url: "{{ url('admin/users/store') }}",
                data: data,
                dataType: "json",
                success: function (response) {
                    $("input", $("#create-user-form")).val('');
                    $.colorbox.close();
                    var token = response.token;
                    $("#delete-token").val(token);
                    $("input[name='_token']", $("#create-user-form")).val(token);
                },
                error: function (jqXhr) {
                    if (jqXhr.status === 401) {
                        $(location).prop('pathname', 'auth/login');
                    }
                    if (jqXhr.status === 422) {
                        var errors = jqXhr.responseJSON;
                        
                        var errorsHtml = '<div class="alert alert-danger alert-dismissable fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Stäng"><span aria-hidden="true">×</span></button>Det fanns några fel i formuläret.<br><ul>';

                        $.each(errors, function (key, value) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });

                        errorsHtml += '</ul></div>';

                        $("#create-user-form").prepend(errorsHtml);
                    }
                }
            });
        });

        $(".confirm-delete").click(function (ev) {
            ev.preventDefault();
            var id = $(".user-id").val();
            var url = "{{ url('admin/users/---/delete') }}";
            var token = $("#delete-token").val();
            var data = {"_token": token};
            $.ajax({
                type: 'POST',
                url: url.replace("---", id),
                data: data,
                success: function (response) {
                    $.colorbox.close();
                    var token = response.token;
                    $("#delete-token").val(token);
                    $("input[name='_token']", $("#create-user-form")).val(token);
                },
            })
        });
        $(".cancel-delete").click(function (ev) {
            ev.preventDefault();
            $.colorbox.close();
        });

        var sections = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.nonword(d.value)
            },
            queryTokenizer: Bloodhound.tokenizers.nonword,
            identify: function (obj) { return obj.value; },
            remote: {
                url: "{{ url('admin/sections/find/%QUERY') }}",
                wildcard: "%QUERY",
                filter: function (response) {
                    var tagged_section = $('#sections-list').tokenfield('getTokens');
                    return $.map(response, function (section) {
                        var exists = false;
                        for (i=0; i < tagged_section.length; i++) {
                            if (section.id == tagged_section[i].value) {
                                var exists = true;
                            }
                        }
                        if (!exists) {
                            return {
                                value: section.id,
                                label: section.name
                            };
                        }
                    });
                }
            }
        });
        sections.initialize();

        $("#sections-list").tokenfield({
            typeahead: [
                {
                   hint: false, 
                }, { name: 'sections', displayKey: 'label', source: sections.ttAdapter() }]
        })
        .on('tokenfield:createtoken', function (e) {
            var existingTokens = $(this).tokenfield('getTokens');
            if (existingTokens.length) {
                $.each(existingTokens, function(index, token) {
                    if (token.value === e.attrs.value) {
                        e.preventDefault();
                    }
                });
            }
        });
    })
</script>
@stop