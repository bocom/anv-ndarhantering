@extends('admin.layouts.base')

@section('main')
<div id="delete-user-container">
    <div class="container-fluid">
        <div class="page-header">
            <h3>Ta bort användare</h3>
        </div>
        <form method="POST" action="{{ action('Admin\UserController@postDelete', ['id' => $id]) }}">
            <input type="hidden" id="delete-token" name="_token" value="{{ csrf_token() }}">
            <p>Är du säker på att du vill ta bort användaren {{ $user->name }}?</p>
            <div class="form-group">
                <button type="submit" class="confirm-delete btn btn-danger">Ta bort</button>
                <a href="{{ url('admin/users') }}" class="cancel-delete btn btn-default">Avbryt</a>
            </div>
        </form>
    </div>
</div>
@stop