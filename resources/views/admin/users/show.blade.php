@extends('admin.layouts.base')

@section('title') Användare :: @parent @stop

@section('styles')
@parent
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div class="page-header">
    <h3>{{ $user->name }}</h3>
</div>
<strong>E-post:</strong> {{ $user->email }}
<h4>Avdelningar</h4>
<ul>
    @if (count($sections) > 0)
        @foreach($sections as $section)
            <li>{{ $section->name }} <a href="{{ action('Admin\UserController@getRemoveSection', ['sectionId' => $section->id, 'userId' => $id]) }}"><i class="fa fa-trash" title="Ta bort"></i></a></li>
        @endforeach
    @else
        <li>Den här användaren tillhör inte någon avdelning.</li>
    @endif
</ul>

<h4>Lägg till avdelningar</h4>
<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
<input type="text" id="sections-list" class="form-control" name="sections" placeholder="Sök...">
<button type="button" id="add-sections" class="btn btn-primary">Lägg till</button>
@stop

@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var sections = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.nonword(d.value)
        },
        queryTokenizer: Bloodhound.tokenizers.nonword,
        identify: function (obj) { return obj.value; },
        remote: {
            url: "{{ url('admin/sections/find/%QUERY') }}",
            wildcard: "%QUERY",
            filter: function (response) {
                var tagged_section = $('#sections-list').tokenfield('getTokens');
                return $.map(response, function (section) {
                    var exists = false;
                    for (i=0; i < tagged_section.length; i++) {
                        if (section.id == tagged_section[i].value) {
                            var exists = true;
                        }
                    }
                    if (!exists) {
                        return {
                            value: section.id,
                            label: section.name
                        };
                    }
                });
            }
        }
    });
    sections.initialize();

    $("#sections-list").tokenfield({
        typeahead: [
            {
               hint: false, 
            }, { name: 'sections', displayKey: 'label', source: sections.ttAdapter() }]
    })
    .on('tokenfield:createtoken', function (e) {
        var existingTokens = $(this).tokenfield('getTokens');
        if (existingTokens.length) {
            $.each(existingTokens, function(index, token) {
                if (token.value === e.attrs.value) {
                    e.preventDefault();
                }
            });
        }
    });

    $("#add-sections").click(function (ev) {
        ev.preventDefault();

        var token = $("#token").val();
        var sections = $("#sections-list").val();

        $.ajax({
            type: "POST",
            url: "{{ action('Admin\UserController@postAddSections', ['id' => $id]) }}",
            data: { _token: token, sections: sections },
            dataType: "json",
            success: function (response) {
                document.location.reload(true);
            }
        });
    })
</script>
@stop