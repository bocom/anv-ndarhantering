@extends('admin.layouts.base')

@section('main')
<div id="delete-user-container">
    <div class="container-fluid">
        <div class="page-header">
            <h3>Ta bort avdelning</h3>
        </div>
        <form method="POST" action="{{ action('Admin\SectionController@postDelete', ['id' => $id]) }}">
            <input type="hidden" id="delete-token" name="_token" value="{{ csrf_token() }}">
            <p>Är du säker på att du vill ta bort avdelningen "{{ $section->name }}"?</p>
            <div class="form-group">
                <button type="submit" class="confirm-delete btn btn-danger">Ta bort</button>
                <a href="{{ url('admin/section') }}" class="cancel-delete btn btn-default">Avbryt</a>
            </div>
        </form>
    </div>
</div>
@stop