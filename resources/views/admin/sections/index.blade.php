@extends('admin.layouts.base')

@section('title') Avdelningar :: @parent @stop

@section('styles')
@parent
<link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylesheet" href="//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div class="page-header">
    <h3>
        Avdelningar
        <div class="pull-right">
            <a href="{{ url('admin/sections/create') }}" class="btn btn-sm btn-primary inline" title="Ny avdelning">
                <i class="fa fa-user-plus"></i>
                <span class="hidden-sm text">Ny avdelning</span>
            </a>
        </div>
    </h3>
</div>
<table id="table" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Namn</th>
            <th>Antal användare</th>
            <th>Åtgärder</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
@include('admin.ajax.sections.create')
@include('admin.ajax.sections.delete')
@stop

@section('scripts')
@parent
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var oTable;
    $(document).ready(function () {
        oTable = $("#table").DataTable({
            ajax: "{{ url('admin/sections/datatable') }}",
            drawCallback: function () {
                $(".delete-section").colorbox({
                    inline: true,
                    width: 500,
                    height: 275,
                    href: "#delete-section-container",
                    onOpen: function () {
                        var el = $.colorbox.element();
                        var id = el.data('id');
                        var name = el.parent().prev().prev().html();
                        $(".section-name").html(name);
                        $(".section-id").val(id);
                    },
                    onClosed: function () {
                        oTable.ajax.reload();
                    }
                });
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.7/i18n/Swedish.json"
            },
            processing: true,
            responsive: true,
            sDom: "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            serverSide: true,
            columnDefs: [
                { "searchable": false, "targets": 2 }
            ]
        });
        $(".inline").colorbox({
            inline: true,
            width: "40%",
            height: 485,
            href: "#create-section-container",
            onClosed: function () {
                oTable.ajax.reload();
            }
        });
        
        $("#create-section-form").submit(function (ev) {
            ev.preventDefault();

            var data = $("#create-section-form").serialize();

            $.ajax({
                type: 'POST',
                url: "{{ url('admin/sections/store') }}",
                data: data,
                dataType: "json",
                success: function (response) {
                    $("input", $("#create-section-form")).val('');
                    $.colorbox.close();
                    var token = response.token;
                    $("#delete-token").val(token);
                    $("input[name='_token']", $("#create-section-form")).val(token);
                },
                error: function (jqXhr) {
                    if (jqXhr.status === 401) {
                        $(location).prop('pathname', 'auth/login');
                    }
                    if (jqXhr.status === 422) {
                        var errors = jqXhr.responseJSON;
                        
                        var errorsHtml = '<div class="alert alert-danger alert-dismissable fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Stäng"><span aria-hidden="true">×</span></button>Det fanns några fel i formuläret.<br><ul>';

                        $.each(errors, function (key, value) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });

                        errorsHtml += '</ul></div>';

                        $("#create-section-form").prepend(errorsHtml);
                    }
                }
            });
        });

        $(".confirm-delete").click(function (ev) {
            ev.preventDefault();
            var id = $(".section-id").val();
            var url = "{{ url('admin/sections/---/delete') }}";
            var token = $("#delete-token").val();
            var data = {"_token": token};
            $.ajax({
                type: 'POST',
                url: url.replace("---", id),
                data: data,
                success: function (response) {
                    $.colorbox.close();
                    var token = response.token;
                    $("#delete-token").val(token);
                    $("input[name='_token']", $("#create-section-form")).val(token);
                },
            })
        });
        $(".cancel-delete").click(function (ev) {
            ev.preventDefault();
            $.colorbox.close();
        });

        var users = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.nonword(d.value)
            },
            queryTokenizer: Bloodhound.tokenizers.nonword,
            identify: function (obj) { return obj.value; },
            remote: {
                url: "{{ url('admin/users/find/%QUERY') }}",
                wildcard: "%QUERY",
                filter: function (response) {
                    var tagged_user = $('#users-list').tokenfield('getTokens');
                    return $.map(response, function (user) {
                        var exists = false;
                        for (i=0; i < tagged_user.length; i++) {
                            if (user.id == tagged_user[i].value) {
                                var exists = true;
                            }
                        }
                        if (!exists) {
                            return {
                                value: user.id,
                                label: user.name
                            };
                        }
                    });
                }
            }
        });
        users.initialize();

        $("#users-list").tokenfield({
            typeahead: [
                {
                   hint: false, 
                }, { name: 'users', displayKey: 'label', source: users.ttAdapter() }]
        })
        .on('tokenfield:createtoken', function (e) {
            var existingTokens = $(this).tokenfield('getTokens');
            if (existingTokens.length) {
                $.each(existingTokens, function(index, token) {
                    if (token.value === e.attrs.value) {
                        e.preventDefault();
                    }
                });
            }
        });
    })
</script>
@stop