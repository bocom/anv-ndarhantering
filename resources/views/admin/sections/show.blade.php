@extends('admin.layouts.base')

@section('title') Avdelning :: @parent @stop

@section('styles')
@parent
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div class="page-header">
    <h3>{{ $section->name }}</h3>
</div>
<h4>Användare</h4>
<ul id="users">
    @if (count($users) > 0)
        @foreach($users as $user)
            <li>{{ $user->name }} <a href="{{ action('Admin\SectionController@getRemoveUser', ['sectionId' => $id, 'userId' => $user->id]) }}"><i class="fa fa-trash" title="Ta bort"></i></a></li>
        @endforeach
    @else
        <li>Den här avdelningen har inga användare.</li>
    @endif
</ul>

<h4>Lägg till användare</h4>
<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
<input type="text" id="users-list" class="form-control" name="users" placeholder="Sök...">
<button type="button" id="add-users" class="btn btn-primary">Lägg till</button>
@stop

@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var users = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.nonword(d.value)
        },
        queryTokenizer: Bloodhound.tokenizers.nonword,
        identify: function (obj) { return obj.value; },
        remote: {
            url: "{{ url('admin/users/find/%QUERY') }}",
            wildcard: "%QUERY",
            filter: function (response) {
                var tagged_user = $('#users-list').tokenfield('getTokens');
                return $.map(response, function (user) {
                    var exists = false;
                    for (i=0; i < tagged_user.length; i++) {
                        if (user.id == tagged_user[i].value) {
                            var exists = true;
                        }
                    }
                    if (!exists) {
                        return {
                            value: user.id,
                            label: user.name
                        };
                    }
                });
            }
        }
    });
    users.initialize();

    $("#users-list").tokenfield({
        typeahead: [
            {
               hint: false, 
            }, { name: 'users', displayKey: 'label', source: users.ttAdapter() }]
    })
    .on('tokenfield:createtoken', function (e) {
        var existingTokens = $(this).tokenfield('getTokens');
        if (existingTokens.length) {
            $.each(existingTokens, function(index, token) {
                if (token.value === e.attrs.value) {
                    e.preventDefault();
                }
            });
        }
    });

    $("#add-users").click(function (ev) {
        ev.preventDefault();

        var token = $("#token").val();
        var users = $("#users-list").val();

        $.ajax({
            type: "POST",
            url: "{{ action('Admin\SectionController@postAddUsers', ['id' => $id]) }}",
            data: { _token: token, users: users },
            dataType: "json",
            success: function (response) {
                document.location.reload(true);
            }
        });
    })
</script>
@stop