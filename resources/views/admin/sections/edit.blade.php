@extends('admin.layouts.base')

@section('main')
<div id="create-section-container">
    <div class="container-fluid">
        <div class="page-header">
            <h3>Ändra avdelning</h3>
        </div>
        @include('errors.list')

        <form id="edit-section-form" class="form-horizontal" role="form" method="POST" action="{{ action('Admin\SectionController@postEdit', ['id' => $id]) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Namn</label>

                <div class="col-md-6">
                    <input type="text" name="name" class="form-control" value="{{ old('name') ? old('name') : $section->name }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-5">
                    <button type="submit" class="btn btn-primary">Ändra</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop