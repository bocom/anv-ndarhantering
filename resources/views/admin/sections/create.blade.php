@extends('admin.layouts.base')

@section('styles')
@parent
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/tokenfield-typeahead.min.css">
@stop

@section('main')
<div id="create-section-container">
    <div class="container-fluid">
        <div class="page-header">
            <h3>Skapa avdelning</h3>
        </div>
        @include('errors.list')

        <form id="create-section-form" class="form-horizontal" role="form" method="POST" action="{{ url('admin/sections/store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Namn</label>

                <div class="col-md-6">
                    <input type="text" name="name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <h4>Användare</h4>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <input type="text" id="users-list" name="users" placeholder="Sök...">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-5">
                    <button type="submit" class="btn btn-primary">Skapa</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.min.js"></script>
<script>
    var users = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.nonword(d.value)
        },
        queryTokenizer: Bloodhound.tokenizers.nonword,
        identify: function (obj) { return obj.value; },
        remote: {
            url: "{{ url('admin/users/find/%QUERY') }}",
            wildcard: "%QUERY",
            filter: function (response) {
                var tagged_user = $('#users-list').tokenfield('getTokens');
                return $.map(response, function (user) {
                    var exists = false;
                    for (i=0; i < tagged_user.length; i++) {
                        if (user.id == tagged_user[i].value) {
                            var exists = true;
                        }
                    }
                    if (!exists) {
                        return {
                            value: user.id,
                            label: user.name
                        };
                    }
                });
            }
        }
    });
    users.initialize();

    $("#users-list").tokenfield({
        typeahead: [
            {
               hint: false, 
            }, { name: 'users', displayKey: 'label', source: users.ttAdapter() }]
    })
    .on('tokenfield:createtoken', function (e) {
        var existingTokens = $(this).tokenfield('getTokens');
        if (existingTokens.length) {
            $.each(existingTokens, function(index, token) {
                if (token.value === e.attrs.value) {
                    e.preventDefault();
                }
            });
        }
    });
</script>
@stop