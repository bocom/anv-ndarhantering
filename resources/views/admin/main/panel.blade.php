@extends("admin.layouts.base")

@section('main')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5 well well-sm">
            <h4>Nyligen skapade användare</h4>
            <ul>
                @foreach ($users as $user)
                    <li><a href="{{ url('admin/users/' . $user->id) }}">{{ $user->name }} <small>{{ $user->created_at }}</small></a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-5 col-md-offset-1 well well-sm">
            <h4>Nyligen skapade avledningar</h4>
            <ul>
                @foreach ($sections as $section)
                    <li><a href="{{ url('admin/sections/' . $section->id) }}">{{ $section->name }} <small>{{ $section->created_at }}</small></a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@stop