@extends('base')

@section('content')
<div class="row">
    @yield('top')
</div>
<div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
        <div class="list-group">
            <ul class="nav nav-pills nav-stacked">
                <li class="sidebar-search">
                    <div class="input-group search-form">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="text" id="sidebar-search" class="form-control" placeholder="Sök...">
                    </div>
                </li>
                <li class="{{ is_active('/') }}">
                    <a href="{{ url('/') }}">
                        <i class="fa fa-tachometer"></i> 
                        <span class="text">Panel</span>
                    </a>
                </li>
                <li class="{{ is_active('admin/users*') }}">
                    <a href="{{ url('admin/users') }}">
                        <i class="fa fa-users"></i> 
                        <span class="text">Användare</span>
                    </a>
                </li>
                <li class="{{ is_active('admin/sections*') }}">
                    <a href="{{ url('admin/sections') }}">
                        <i class="fa fa-building"></i> 
                        <span class="text">Avdelningar</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10 main">
        @yield('main')
    </div>
</div>
@stop

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.21/jquery.autocomplete.min.js"></script>
<script>
    $(document).ready(function () {
        $("#sidebar-search").autocomplete({
            serviceUrl: "{{ url('admin/search') }}",
            deferRequestBy: 150,
            groupBy: "type_label",
            onSelect: function (suggestion) {
                var type = suggestion.data.type;
                var id = suggestion.data.id;
                var url = "";
                if (type == "section") {
                    url = "{{ url('admin/sections/') }}/";
                }
                else {
                    url = "{{ url('admin/users/') }}/";
                }
                location.href = url + id;
            }
        });
    });
</script>
@stop