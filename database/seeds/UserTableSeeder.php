<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => "Administratör",
            'email' => "admin@foretag.se",
            'password' => bcrypt("admin"),
            'admin' => true,
        ]);

        
    }
}
