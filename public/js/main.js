(function() {
    String.prototype.pick = function(min, max) {
        var n, chars = '';

        if (typeof max === 'undefined') {
            n = min;
        } else {
            n = min + Math.floor(Math.random() * (max - min));
        }

        for (var i = 0; i < n; i++) {
            chars += this.charAt(Math.floor(Math.random() * this.length));
        }

        return chars;
    };

    // Credit to @Christoph: http://stackoverflow.com/a/962890/464744
    String.prototype.shuffle = function() {
        var array = this.split('');
        var tmp, current, top = array.length;

        if (top) while (--top) {
            current = Math.floor(Math.random() * (top + 1));
            tmp = array[current];
            array[current] = array[top];
            array[top] = tmp;
        }

        return array.join('');
    };

    $("#generate-password").click(function (ev) {
        var specials = '!@#$%^&*_+"?';
        var lowercase = 'abcdefghijklmnopqrstuvwxyz';
        var uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var numbers = '0123456789';

        var all = specials + lowercase + uppercase + numbers;

        var password = '';
        password += specials.pick(1);
        password += lowercase.pick(1);
        password += uppercase.pick(1);
        password += all.pick(3, 13);
        password = password.shuffle();

        $("#generated-password").val(password);
        $("input[name='password'], input[name='password_confirmation']").val(password);
    });
})();