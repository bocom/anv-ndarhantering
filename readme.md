## Användarhantering

### Systemkrav
* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension

### Installeringsinstruktioner

#### Med SSH/kommandotolk
För detta krävs det att servern/datorn har [Composer](https://getcomposer.org/download/) installerat.

1. Kör kommandot `composer install` i projektets rot för att installera alla bibliotek som krävs.
2. Kopiera `.env.example` till `.env` och ändra på `APP_KEY` till en slumpmässig sträng samt ändra databasinställningar.
3. Kör kommandot `php artisan migrate` för att skapa tabellerna i databasen.
4. (Valfritt) Om testdata önskas, kör kommandot `php artisan db:seed`.

#### Utan SSH
1. Ladda ner [manual_install.7z](https://bitbucket.org/bocom/anv-ndarhantering/downloads/manual_install.7z) och packa upp innehållet i projektets rotmapp.
2. Öppna `.env` och ändra på `APP_KEY` till en slumpmässig sträng samt ändra databasinställningar.
3. Importera `structure.sql` till databasen för att skapa tabellerna.
4. (Valfritt) Om testdata önskas, importera `seed.sql` till databasen.

#### Slutsteg
Mappen `public` är ingångpunkten till projektet. Ifall webbservern förväntar sig ett namn annat än `public`, byt namn på den.
Ifall resten av projektet inte kommer ligga bredvid `public` mappen, följ instruktionerna under Projektstruktur.

Mapparna `storage` (och dess undermappar) samt `bootstrap/cache` behöver skrivrättigheter.

Öppna `config/app.php` och ändra `url` ifall servern inte körs lokalt.

**Administratörinloggning**

* Användarnamn: admin@foretag.se
* Lösenord: admin

#### Projektstruktur
Ifall innehållet i `public` flyttas in i en annan mapp måste `APP_PATH` sökvägen i `public/index.php` ändras.

Alternativt vill man kanske att projektets rot ligger i en undermapp. t.ex. på följande vis:
~~~~
/
|-- application
|    |-- app
|    |-- config
|    +-- ...
+-- public
     |-- css
     |-- js
     |-- ...
~~~~

Det enda som behövs ändras på är `APP_PATH`. så att den pekar till projektets rot. I det här exemplet skulle APP_PATH se ut så här:

```
#!php
define("APP_PATH", __DIR__ . '/../application');
```