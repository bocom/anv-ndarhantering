<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Contracts\Routing\ResponseFactory;

class Admin implements Middleware
{
    protected $auth;

    protected $response;

    function __construct(Guard $auth, ResponseFactory $response)
    {
        $this->auth = $auth;
        $this->response = $response;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $admin = $this->auth->user()->admin;
            
            if (!$admin) {
                return $this->response->redirectTo('/');
            }

            return $next($request);
        }

        return $this->response->redirectTo('/');
    }
}