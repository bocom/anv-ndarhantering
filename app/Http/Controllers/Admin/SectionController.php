<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Section;
use App\Http\Requests\Admin\SectionRequest;
use App\Http\Requests\Admin\SectionEditRequest;
use Datatables;
use Illuminate\Http\Request;

class SectionController extends AdminController
{
    public function getIndex()
    {
        return view('admin.sections.index');
    }

    public function getData()
    {
        $sections = Section::all();

        $result = [];
        foreach ($sections as $section) {
            $result[] = [
                "id" => $section->id,
                "name" => $section->name
            ];
        }
        return $result;
    }

    public function getFind($query)
    {
        $query = "%$query%";
        $result = [];
        $sections = Section::where('name', 'LIKE', $query);

        foreach ($sections->get() as $section) {
            $result[] = [
                "id" => $section->id,
                "name" => $section->name
            ];
        }
        return $result;
    }

    public function getDatatable()
    {
        $sections = Section::select(array('id', 'name'));

        return Datatables::of($sections)
            ->addColumn('count', '{{ \App\Section::find($id)->users->count() }}')
            ->addColumn('actions', '<a href="{{ url(\'admin/sections/\' . $id) }}" class="btn btn-sm btn-info"><i class="fa fa-users"></i>Användare</a><a href="{{ url(\'admin/sections/\' . $id . \'/edit\') }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i>Ändra</a><a href="{{ url(\'admin/sections/\' . $id . \'/delete\') }}" data-id="{{ $id }}" class="delete-section btn btn-sm btn-danger"><i class="fa fa-trash"></i>Ta bort</a>')
            ->removeColumn('id')
            ->make();
    }

    public function getShow($id)
    {
        $section = Section::find($id);
        $users = $section->users;
        return view('admin.sections.show', compact('id', 'section', 'users'));
    }

    public function getCreate()
    {
        return view('admin.sections.create');
    }

    public function postStore(SectionRequest $request)
    {
        $section = new Section;
        $section->name = $request->input('name');
        $section->save();

        $usersString = $request->input('users');
        if (!empty($usersString)) {
            $usersString = str_replace("+", "", $usersString);
            $users = explode(",", $usersString);
            $section->users()->attach($users);
        }

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/sections');
    }

    public function getEdit($id)
    {
        $section = Section::find($id);
        return view('admin.sections.edit', compact("id", "section"));
    }

    public function postEdit(SectionRequest $request, $id)
    {
        $section = Section::find($id);
        $section->name = $request->input('name');
        $section->save();

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/sections');
    }

    public function getDelete($id)
    {
        $section = Section::find($id);
        return view('admin.sections.delete', compact("id", "section"));
    }

    public function postDelete(Request $request, $id)
    {
        $section = Section::find($id);
        $section->delete();

        $sectionUsers = \App\SectionUser::where('section_id', $id)->get();
        foreach ($sectionUsers as $sectionUser) {
            $sectionUser->delete();
        }

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/sections');
    }

    public function postAddUsers(Request $request, $id)
    {
        $section = Section::find($id);

        $usersString = $request->input('users');
        if (!empty($usersString)) {
            $usersString = str_replace("+", "", $usersString);
            $users = explode(",", $usersString);
            foreach ($users as $user) {
                $query = \App\SectionUser::where('section_id', $id)
                    ->where('user_id', $user)
                    ->first();
                if (is_null($query)) {
                    $section->users()->attach($user);
                }
            }
        }

        return ["status" => "success"];
    }

    public function getRemoveUser(Request $request, $sectionId, $userId)
    {
        \App\SectionUser::where('section_id', $sectionId)
            ->where('user_id', $userId)
            ->delete();

        return redirect()->action("Admin\SectionController@getShow", ['id' => $sectionId]);
    }
}