<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use Datatables;
use Illuminate\Http\Request;

class UserController extends AdminController
{
    public function getIndex()
    {
        return view('admin.users.index');
    }

    public function getData()
    {
        $users = User::all();

        $result = [];
        foreach ($users as $user) {
            $result[] = [
                "value" => $user->id,
                "text" => $user->name
            ];
        }
        return $result;
    }

    public function getFind($query)
    {
        $query = "%$query%";
        $result = [];
        $users = User::where('name', 'LIKE', $query)
            ->orWhere('email', 'LIKE', $query);

        foreach ($users->get() as $user) {
            $result[] = [
                "id" => $user->id,
                "name" => $user->name
            ];
        }
        return $result;
    }

    public function getDatatable()
    {
        $users = User::select(array('id', 'name', 'email'));

        return Datatables::of($users)
            ->add_column('actions', '@if ($id != 1)<a href="{{ url(\'admin/users/\' . $id) }}" class="btn btn-sm btn-info"><i class="fa fa-building"></i>Avdelningar</a><a href="{{ url(\'admin/users/\' . $id . \'/edit\') }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i>Ändra</a><a href="{{ url(\'admin/users/\' . $id . \'/delete\') }}" data-id="{{ $id }}" class="delete-user btn btn-sm btn-danger"><i class="fa fa-trash"></i>Ta bort</a>@endif')
            ->remove_column('id')
            ->make();
    }

    public function getShow($id)
    {
        $user = User::find($id);
        $sections = $user->sections;
        return view('admin.users.show', compact('id', 'user', 'sections'));
    }

    public function getCreate()
    {
        return view('admin.users.create');
    }

    public function postStore(UserRequest $request)
    {
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $sectionsString = $request->input('sections');
        if (!empty($sectionsString)) {
            $sectionsString = str_replace("+", "", $sectionsString);
            $sections = explode(",", $sectionsString);
            $user->sections()->attach($sections);
        }

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/users');
    }

    public function getEdit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', compact("id", "user"));
    }

    public function postEdit(UserEditRequest $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/users');
    }

    public function getDelete($id)
    {
        $user = User::find($id);
        return view('admin.users.delete', compact("id", "user"));
    }

    public function postDelete(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();

        $sectionUsers = \App\SectionUser::where('user_id', $id)->get();
        foreach ($sectionUsers as $sectionUser) {
            $sectionUser->delete();
        }

        if ($request->ajax()) {
            $newToken = str_random(40);
            $request->session()->forget('_token');
            $request->session()->put('_token', $newToken);
            return ["status" => "success", "token" => $newToken];
        }

        return redirect()->to('admin/users');
    }

    public function postAddSections(Request $request, $id)
    {
        $user = User::find($id);

        $sectionsString = $request->input('sections');
        if (!empty($sectionsString)) {
            $sectionsString = str_replace("+", "", $sectionsString);
            $sections = explode(",", $sectionsString);
            foreach ($sections as $section) {
                $query = \App\SectionUser::where('section_id', $section)
                    ->where('user_id', $id)
                    ->first();
                if (is_null($query)) {
                    $user->sections()->attach($section);
                }
            }
        }

        return ["status" => "success"];
    }

    public function getRemoveSection(Request $request, $sectionId, $userId)
    {
        \App\SectionUser::where('section_id', $sectionId)
            ->where('user_id', $userId)
            ->delete();

        return redirect()->action("Admin\UserController@getShow", ['id' => $sectionId]);
    }
}