<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }

    public function authorize()
    {
        return true;
    }
}