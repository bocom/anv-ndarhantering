<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $user = Auth::user();
    if (is_null($user) || !$user->admin) {
        return view('main.home');
    }
    $sections = App\Section::orderBy('created_at', 'DESC')->take(5)->get();
    $users = App\User::orderBy('created_at', 'DESC')->take(5)->get();
    return view('admin.main.panel', compact('sections', 'users'));
});

Route::get('about', function () {
    return view('main.about');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {
    // Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', "UserController@getIndex");
        Route::get('/find/{query}', "UserController@getFind");
        Route::get('/data', "UserController@getData");
        Route::get('/datatable', "UserController@getDatatable");

        Route::get('/create', "UserController@getCreate");
        Route::post('/store', "UserController@postStore");

        Route::get('/{id}', "UserController@getShow");
        
        Route::get('/{id}/edit', "UserController@getEdit");
        Route::post('/{id}/update', "UserController@postEdit");

        Route::get('/{id}/delete', "UserController@getDelete");
        Route::post('/{id}/delete', "UserController@postDelete");

        Route::post('/{id}/add_sections', "UserController@postAddSections");
        Route::get('/{userId}/remove_section/{sectionId}', "UserController@getRemoveSection");
    });

    // Sections
    Route::group(['prefix' => 'sections'], function () {
        Route::get('/', "SectionController@getIndex");
        Route::get('/find/{query}', "SectionController@getFind");
        Route::get('/data', "SectionController@getData");
        Route::get('/datatable', "SectionController@getDatatable");

        Route::get('/create', "SectionController@getCreate");
        Route::post('/store', "SectionController@postStore");

        Route::get('/{id}', "SectionController@getShow");
        
        Route::get('/{id}/edit', "SectionController@getEdit");
        Route::post('/{id}/update', "SectionController@postEdit");

        Route::get('/{id}/delete', "SectionController@getDelete");
        Route::post('/{id}/delete', "SectionController@postDelete");

        Route::post('/{id}/add_users', "SectionController@postAddUsers");
        Route::get('/{sectionId}/remove_user/{userId}', "SectionController@getRemoveUser");
    });

    // Search
    Route::get('/search', function (Illuminate\Http\Request $request) {
        $query = '%' . $request->input('query') . '%';

        $suggestions = [];
        $usersQuery = \App\User::where('name', 'LIKE', $query)
            ->orWhere('email', 'LIKE', $query);
        foreach ($usersQuery->get() as $user) {
            $suggestions[] = [
                'value' => $user->name,
                'data' => [
                    'id' => $user->id,
                    'type_label' => "Användare",
                    'type' => "user"
                ]
            ];
        }
        $sectionsQuery = \App\Section::where('name', 'LIKE', $query);
        foreach ($sectionsQuery->get() as $section) {
            $suggestions[] = [
                'value' => $section->name,
                'data' => [
                    'id' => $section->id,
                    'type_label' => 'Avdelningar',
                    'type' => "section"
                ]
            ];
        }

        return compact("suggestions");
    });
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

