<?php

/**
 * Returns a string if the specified route is active.
 *
 * @param $route The route to test
 * @param string $str The string to return
 * @return string
 */
function is_active($route, $str = 'active') {
    return call_user_func_array('Request::is', (array) $route) ? $str : '';
}

/**
 * Breaks execution of the script and prints all arguments.
 */
function b()
{
    echo '<pre>';
    array_map(function($x) { var_dump($x); }, func_get_args());
    echo '</pre>';
    die;
}